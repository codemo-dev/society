document.addEventListener('DOMContentLoaded',_=>{
	console.log('Begin');
	const simulateButton = document.getElementById('simulate');
	const simulationArea = document.getElementById('world');

	simulateButton.addEventListener('click',_=>{
		const simulation = new Simulator(50, simulationArea);
	})



const fps = 25;
const oneYearInMs = 200;

class Simulator{
	constructor(items, container){
		this.world = container;
		console.log("World: ", this.world);
		this.simulationState = -1;
		this.persons = Array(items).fill(0).map(_=>new Person());
		this.createWorld();
		this.tickId = setInterval(_=>{
			this.loop();
		}, 1000/fps);
		this.tick = setInterval(_=>{
			this.liveCicle();
		}, oneYearInMs);

		const rand = Math.floor(Math.random() * this.persons.length-1);
		console.log(rand);
		this.persons[rand].health = 0;

		this.createWorld();
	}

	createWorld(){
		const wordSize = this.world.getBoundingClientRect();
		this.world.textContent = '';
		for (let index = 0; index < this.persons.length; index++) {
			let div = document.createElement('div');
			div.classList.add('person');
			div.setAttribute('data-id', index);

			this.persons[index].position.x =  Math.floor(Math.random()*wordSize.width-10)+10;
			this.persons[index].position.y =  Math.floor(Math.random()*wordSize.height-10)+10;
			this.persons[index].id = index;
			
			div.style.left = `${this.persons[index].position.x}px`;
			div.style.top = `${this.persons[index].position.y}px`;
			this.world.appendChild(div);
		}
	}

	loop(){
		const wordSize = this.world.getBoundingClientRect();
		for (let index = 0; index < this.persons.length; index++) {
			const person = this.persons[index];
			if(person.age===-1){
				this.die(person.id);
				this.persons.splice(index,1);
				continue;
			}
			this.persons[index].move(wordSize.width, wordSize.height);		
			this.detectColision(this.persons[index]);	
			const elem = document.querySelector(`[data-id="${this.persons[index].id}"]`);
			elem.style.left = `${this.persons[index].position.x}px`;
			elem.style.top = `${this.persons[index].position.y}px`;
			if(this.persons[index].health<=0){
				console.log('red');
				elem.style.backgroundColor = 'red';
			}
		}
	}

	detectColision (current) {
		// we are using multiplications because it's faster than calling Math.pow
		for (let i = 0; i < this.persons.length; i++) {
			if(current.id !=this.persons[i].id && current.health==0){
				var distance = Math.sqrt((current.position.x - this.persons[i].position.x) * (current.position.x - this.persons[i].position.x) +
				(current.position.y - this.persons[i].position.y) * (current.position.y - this.persons[i].position.y) );
				if(distance<20){
					console.log('trafiony');
					this.persons[i].health--;
					console.log(this.persons[i]);
				}
			}
		}
	  }

	die(id){
		const elem = document.querySelector(`[data-id="${id}"]`);
		elem.parentNode.removeChild(elem);
		console.log('delete: ', id);
	}

	liveCicle(){
		for (let index = 0; index < this.persons.length; index++) {
			this.persons[index].live();
		}
	}
}

class Person{
	constructor(){
		this.position = {
			x: null,
			y: null, 
			speedX: Math.floor(Math.random() * (25 - 10 + 1)) + 10,
			speedY: Math.floor(Math.random() * (25 - 10 + 1)) + 10,
			directionX : 1,
			directionY : 1,
		}
		this.age = Math.floor(Math.random() * (75 - 10 + 1)) + 10;
		this.id = -1;
		
		this.diseases = [];

		this.health = 1;
		this.nature = {

		}
	}
	live (){
		if(this.age>=100){
			clearInterval(this.liveTick);
			this.age = -1;
		}else{
			this.age++;
		}
	}
	move(wordWidth,wordHeight){

		if(this.position.x <=0 || this.position.x >=wordWidth){
			this.position.speedX = Math.floor(Math.random() * (25 - 10 + 1)) + 10 ;
			this.position.directionX *= -1 ;
		}

		if(this.position.y <=0 || this.position.y >=wordHeight){
			this.position.speedY = Math.floor(Math.random() * (25 - 10 + 1)) + 10 ;
			this.position.directionY *= -1 ;
		}

		this.position.x+=(this.position.speedX*this.position.directionX);
		this.position.y+=(this.position.speedY*this.position.directionY);
	}
}




})